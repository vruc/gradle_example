package org.gradle.example.simple;

import org.gradle.example.simple.MathUtil;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestMathUtil {

	@Test
	public void testAdd(){
		
		int x = 1;
		int y = 2;

		int sum = MathUtil.add(x, y);

		assertEquals(3, sum);

	}

}