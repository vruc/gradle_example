package org.gradle.example.simple;

public class MathUtil {

	private MathUtil() {}

	public static int add(int x, int y) {
		return x + y;
	}
}