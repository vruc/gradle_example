package org.gradle.example;

import org.gradle.example.Utility;
import org.gradle.example.HttpUtil;

import java.io.IOException;

public class HelloWorld {

	public static void main(String[] args){
		
		try {
			System.out.println("Hello World");
			System.out.println(Utility.add(1, 2));
			System.out.println(HttpUtil.get("http://1.stageing.sinaapp.com/welcome/datetime"));	
		} catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
		
	}

}
